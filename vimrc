ab teh the

syntax on
set shiftwidth=2
set smartindent
set tabstop=2
set softtabstop=2
set smarttab
set number
set hlsearch

set foldmethod=syntax
set foldlevelstart=10
set tw=79
set colorcolumn=80
set splitright
set splitbelow
set cino=N-s

nnoremap \p :set paste!<CR>
nnoremap \P :set nopaste!<CR>

if has('python3')
endif
"if exists('py2') && has('python')
"elseif has('python3')
"endif

call plug#begin('~/.vim/plugged')
Plug 'Shougo/unite.vim'
Plug 'scrooloose/nerdtree'
"--Linter
Plug 'scrooloose/syntastic'
Plug 'bling/vim-airline'
Plug 'Lokaltog/vim-powerline'
Plug 'tpope/vim-fugitive'
"Make vim an IDE for Python
"Plug 'klen/python-mode'
"--Use the jedi autocompletion library for vim
Plug 'davidhalter/jedi-vim'
Plug 'sjl/gundo.vim' 
Plug 'majutsushi/tagbar'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'fatih/vim-go'
Plug 'alvan/vim-closetag'
Plug 'tmhedberg/matchit'
Plug 'nsf/gocode', { 'rtp': 'vim', 'do': '~/.vim/plugged/gocode/vim/symlink.sh' }
Plug 'easymotion/vim-easymotion'
"Plug 'OmniSharp/omnisharp-vim'
Plug 'Valloric/YouCompleteMe'
"Plug 'w0rp/ale'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"--Expand abbreviations
Plug 'mattn/emmet-vim'
"--Jump to dependencies for node.js files (JS and TS)
Plug 'moll/vim-node'
Plug 'Quramy/tsuquyomi'
"-- Typescript syntax coloring
Plug 'leafgarland/typescript-vim'
"--Indentation for JS and TS. NOTE: I haven't seen the effect of this yet -_-
Plug 'jason0x43/vim-js-indent'
call plug#end()

set laststatus=2

let NERDTreeChDirMode=2
let NERDTreeIgnore=['\.vim$', '\~$', '\.pyc$', '\.swp$', '\.o$', '\.log$', '\.h\.gch']
let NERDTreeSortOrder=['^__\.py$', '\/$', '*', '\.swp$',  '\~$']
let NERDTreeShowBookmarks=1
map <F2> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

nmap <F8> :TagbarToggle<CR>
let g:airline#extensions#tabline#enabled = 1

filetype plugin indent on
augroup vimrc_autocmds
    autocmd!
    " highlight characters past column 120
    autocmd FileType python highlight Excess ctermbg=DarkGrey guibg=Black
    autocmd FileType python match Excess /\%120v.*/
    autocmd FileType python set nowrap
    augroup END

" Python-mode
" Activate rope
" Keys:
" K             Show python docs
" <Ctrl-Space>  Rope autocomplete
" <Ctrl-c>g     Rope goto definition
" <Ctrl-c>d     Rope show documentation
" <Ctrl-c>f     Rope find occurrences
" <Leader>b     Set, unset breakpoint (g:pymode_breakpoint enabled)
" [[            Jump on previous class or function (normal, visual, operator modes)
" ]]            Jump on next class or function (normal, visual, operator modes)
" [M            Jump on previous class or method (normal, visual, operator modes)
" ]M            Jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 0

" Documentation
let g:pymode_doc = 1
let g:pymode_doc_key = 'K'

"Linting
let g:pymode_python = 'python3'
let g:pymode_lint = 1
let g:pymode_lint_checker = "pylint,pep8,mccabe"
" Auto check on save
let g:pymode_lint_write = 1

" Support virtualenv
let g:pymode_virtualenv = 1

" Enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_key = '<leader>b'

" syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" Don't autofold code
let g:pymode_folding = 0

au FileType py set expandtab
au FileType py set textwidth=79 " PEP-8 Friendly
au FileType py set list listchars=tab:▷⋅,trail:⋅,nbsp:⋅
let g:pydiction_location = '/home/user/.vim/bundle/pydiction/complete-dict' 


"-------------------------------------------
let g:jedi#use_splits_not_buffers = "left" 



"--------------------------------------------
"Tabar
 nmap <F8> :TagbarToggle<CR>

"-----------------------------------------Omnisharp config
"source /home/judi/.vim/omnisharprc

"------------------------------------------------------
"ALE
"let g:ale_fixers = {
"	\	'*': ['remove_trailing_lines', 'trim_whitespace'],
"  \ 'javascript': ['prettier', 'eslint'],
"	\ 'typescript': ['tslint']
"  \ }

